package main

import (
	"path/filepath"

	"codeberg.org/h7c/savecli/app"
	"codeberg.org/h7c/savecli/cmd"
	"github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"
)

func main() {
	hd, err := homedir.Dir()
	if err != nil {
		logrus.Fatalf("cannot retrieve user's home folder: %s", err)
	}
	app.BookmarkFolder = filepath.Join(hd, ".savecli")
	cmd.Execute()
}
