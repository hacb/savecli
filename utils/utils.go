package utils

// StringInSlice returns true if s is in sl, false otherwise
func StringInSlice(s string, sl []string) bool {
	for _, v := range sl {
		if v == s {
			return true
		}
	}
	return false
}

// SliceInSlice returns true if sl1 is in sl2 (order not taken into account),
// false otherwise
func SliceInSlice(sl1 []string, sl2 []string) bool {
	for _, i := range sl1 {
		if !StringInSlice(i, sl2) {
			return false
		}
	}
	return true
}
