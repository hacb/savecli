package app

import (
	"errors"
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"codeberg.org/h7c/savecli/utils"
	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	yaml "gopkg.in/yaml.v3"
)

const BookmarkFileName = "bookmarks.yaml"

var BookmarkFolder string

// TODO: add Description and Title fields that are fetched from the link
// TODO:   directly
type Bookmark struct {
	Link  string   `yaml:"link"`
	Title string   `yaml:"title"`
	Tags  []string `yaml:"tags"`
}

type Data struct {
	Bookmarks []Bookmark `yaml:"bookmarks"`
}

// NewBookmark returns a new empty bookmark
func NewBookmark() Bookmark {
	return Bookmark{}
}

// Dump dumps the content of b on STDOUT
func (b Bookmark) Dump() {
	fmt.Printf("Title: %s\n", b.Title)
	fmt.Printf("Link: %s\n", b.Link)
	fmt.Println("Tags:")
	for _, t := range b.Tags {
		fmt.Printf("  #%s\n", t)
	}
}

// Dumpln dumps the content of b on STDOUT and adds a new line at the end of the
// dump
func (b Bookmark) Dumpln() {
	fmt.Printf("Title: %s\n", b.Title)
	fmt.Printf("Link: %s\n", b.Link)
	fmt.Println("Tags:")
	for _, t := range b.Tags {
		fmt.Printf("  #%s\n", t)
	}
	fmt.Println()
}

// SaveBookmark saves bookmark in file
func SaveBookmark(b Bookmark) error {
	_, err := url.ParseRequestURI(b.Link)
	if err != nil {
		return fmt.Errorf("cannot parse bookmark: %s is not a valid URL", b.Link)
	}

	d, err := os.ReadFile(filepath.Join(BookmarkFolder, BookmarkFileName))
	if err != nil {
		return err
	}

	var all Data
	if err := yaml.Unmarshal(d, &all); err != nil {
		return err
	}
	all.Bookmarks = append(all.Bookmarks, b)
	out, err := yaml.Marshal(&all)
	if err != nil {
		return err
	}

	return os.WriteFile(filepath.Join(BookmarkFolder, BookmarkFileName), out, 0755)
}

// FindBookmarksWithTags takes an integer that is the maximum number of bookmarks
// to return as well as a list of tags. It returns a list a bookmarks that have
// all the tags given in input, the list having a max length of n.
func FindBookmarksWithTags(n int, tags []string) ([]Bookmark, error) {
	if n <= 0 {
		return nil, errors.New("n cannot be lower than or equal to 0")
	}

	d, err := os.ReadFile(filepath.Join(BookmarkFolder, BookmarkFileName))
	if err != nil {
		return nil, err
	}

	var all Data
	if err := yaml.Unmarshal(d, &all); err != nil {
		return nil, err
	}

	var matchingBookmarks []Bookmark
	for _, b := range all.Bookmarks {
		if utils.SliceInSlice(tags, b.Tags) {
			matchingBookmarks = append(matchingBookmarks, b)
		}
		if len(matchingBookmarks) == n {
			break
		}
	}
	return matchingBookmarks, nil
}

func Save() error {
	r, err := git.PlainOpen(BookmarkFolder)
	if err != nil {
		return fmt.Errorf("plain open: %s", err)
	}

	w, err := r.Worktree()
	if err != nil {
		return fmt.Errorf("worktree: %s", err)
	}

	_, err = w.Add(BookmarkFileName)
	if err != nil {
		return fmt.Errorf("add: %s", err)
	}

	commit, err := w.Commit("savecli: update bookmarks", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "SaveCLI",
			Email: "savecli@codeberg.org",
			When:  time.Now(),
		},
	})
	if err != nil {
		return fmt.Errorf("commit: %s", err)
	}

	_, err = r.CommitObject(commit)
	if err != nil {
		return fmt.Errorf("commit object: %s", err)
	}

	if err = r.Push(&git.PushOptions{}); err != nil {
		return fmt.Errorf("push: %s", err)
	}

	return nil
}
