package cmd

import (
	"fmt"

	"codeberg.org/h7c/savecli/app"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var N int

// findCmd represents the find command
var findCmd = &cobra.Command{
	Use:   "find tags",
	Short: "Find a specific bookmark based on tags",
	// 	Long: `A longer description that spans multiple lines and likely contains examples
	// and usage of using your command. For example:

	// Cobra is a CLI library for Go that empowers applications.
	// This application is a tool to generate the needed files
	// to quickly create a Cobra application.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		bs, err := app.FindBookmarksWithTags(N, args)
		if err != nil {
			logrus.Fatalf("error: find bookmarks with tags: %s", err)
		}

		if len(bs) == 0 {
			fmt.Println("no bookmark found :(")
			return
		}

		for i, b := range bs {
			fmt.Printf("Bookmark #%d:\n", i+1)
			b.Dumpln()
		}
	},
}

func init() {
	rootCmd.AddCommand(findCmd)
	findCmd.Flags().IntVarP(&N, "num", "n", 10, "Number of matching bookmarks to return")
}
