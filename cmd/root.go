package cmd

import (
	"fmt"
	"os"

	"codeberg.org/h7c/savecli/app"
	"codeberg.org/h7c/savecli/parser"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "savecli <url> [tags]",
	Short: "Save and bookmark links from the terminal",
	// 	Long: `A longer description that spans multiple lines and likely contains
	// examples and usage of using your application. For example:

	// Cobra is a CLI library for Go that empowers applications.
	// This application is a tool to generate the needed files
	// to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		// TODO: check that args[0] is a valid link
		b := app.NewBookmark()
		b.Link = args[0]
		b.Tags = args[1:]
		b.Title, err = parser.GetHTMLTitle(b.Link)
		if err != nil {
			logrus.Errorf("error: cannot get url title: %s", err)
		}
		if err := app.SaveBookmark(b); err != nil {
			logrus.Fatalf("error: cannot save bookmark: %s", err)
		}
		fmt.Println("Bookmark added:")
		b.Dump()
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
